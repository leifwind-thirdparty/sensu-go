FROM golang:alpine
ADD download.sh /tmp/download.sh
WORKDIR /tmp
RUN ./download.sh 5.16.1
RUN tar -xvf v5.16.1.tar.gz
RUN cd /tmp/sensu-go-5.16.1 && \
        go build -ldflags '-X "github.com/sensu/sensu-go/version.Version=5.16.1" -X "github.com/sensu/sensu-go/version.BuildDate=2020-01-09"' -o bin/sensu-agent ./cmd/sensu-agent && \
        go build -ldflags '-X "github.com/sensu/sensu-go/version.Version=5.16.1" -X "github.com/sensu/sensu-go/version.BuildDate=2020-01-09"' -o bin/sensuctl ./cmd/sensuctl && \
        go build -ldflags '-X "github.com/sensu/sensu-go/version.Version=5.16.1" -X "github.com/sensu/sensu-go/version.BuildDate=2020-01-09"' -o bin/sensu-backend ./cmd/sensu-backend

FROM alpine:latest
WORKDIR /opt/sensu/bin
RUN apk --no-cache add dumb-init ca-certificates
COPY --from=0 /tmp/sensu-go-5.16.1/bin/ /opt/sensu/bin
ADD ./sensu-entrypoint.sh /opt/sensu/bin
RUN ln -sf /opt/sensu/bin/sensu-entrypoint.sh /usr/local/bin/sensu-agent && \
        ln -sf /opt/sensu/bin/sensuctl /usr/local/bin && \
        ln -sf /opt/sensu/bin/sensu-entrypoint.sh /usr/local/bin/sensu-backend

CMD ["sensu-backend"]
