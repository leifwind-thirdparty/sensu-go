#!/usr/bin/dumb-init sh

SENSU=/opt/sensu

called=$(basename $0)
called_path=${SENSU}/bin/${called}

if [ $called = "sensu-backend" ]; then
    export SENSU_BACKEND_CLUSTER_ADMIN_USERNAME="admin"
    export SENSU_BACKEND_CLUSTER_ADMIN_PASSWORD="P@ssw0rd!"
    # wait for port 8080 to become available
    while /bin/true; do
        NC_RC=0
        set -e
        nc -z 127.0.0.1 8080 || NC_RC=$?
        set +e
        if [ "x$NC_RC" = "x0" ]; then
            echo "running backend init..."
            set -e
            ${called_path} init
            INIT_RC=$?
            set +e
            if [ "x$INIT_RC" != "x0" ] && [ "x$INIT_RC" != "x3" ]; then
                echo "backend init failed - exiting..."
                exit 1
            fi
            break
        else
            echo "waiting for backend to become available before running backend-init..."
            sleep 1.0
        fi
    done &
fi

${called_path} $@
