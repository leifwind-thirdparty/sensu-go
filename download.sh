#!/bin/sh
export RELEASE_URL=$(wget -q -O - https://github.com/sensu/sensu-go/releases | grep -E '.*/archive/v'$1'\.tar\.gz' | awk -F"\"" '{print$2}')
wget -q "https://github.com${RELEASE_URL}"
